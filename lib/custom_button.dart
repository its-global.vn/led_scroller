import 'dart:async';

import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget {
  final String imagePath;
  final String onTapImagesPath;
  final GestureTapCallback? onTap;
  final double? height;
  final double? width;
  final double? borderRadius;
  final int? thisButtonIndex;
  final int? checkIndex;
  final bool? setImage;
  final Function()? onLongPressFunction;
  final Function()? onLongPressEndFunction;
  final int? duration;
  const CustomButton({
    Key? key,
    required this.imagePath,
    this.onTap,
    required this.onTapImagesPath,
    this.height,
    this.width,
    this.borderRadius,
    this.thisButtonIndex,
    this.checkIndex,
    this.onLongPressFunction,
    this.setImage,
    this.duration,
    this.onLongPressEndFunction,
  }) : super(key: key);

  @override
  State<CustomButton> createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  bool doTap = false;
  bool check = false;
  Timer? timer;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.checkIndex == widget.thisButtonIndex &&
        widget.checkIndex != null &&
        widget.thisButtonIndex != null) {
      check = true;
    } else {
      check = false;
    }
    return Container(
        height: widget.height ?? MediaQuery.of(context).size.width * 1 / 5.5,
        width: widget.width ?? MediaQuery.of(context).size.width * 1 / 5.5,
        decoration: BoxDecoration(
          borderRadius:
              BorderRadius.all(Radius.circular(widget.borderRadius ?? 1.0)),
        ),
        child: GestureDetector(
          onTapDown: (details) {
            setState(() {
              widget.onLongPressFunction != null ? doTap = true : null;
            });
          },
          onTapUp: (details) {
            Future.delayed(
              Duration(milliseconds: widget.duration ?? 50),
              () {
                setState(() {
                  (widget.setImage ?? false) ? doTap = !doTap : doTap = false;
                  widget.onTap!.call();
                });
              },
            );
          },
          onLongPressStart: (details) {
            timer = Timer.periodic(const Duration(milliseconds: 50), (timer) {
              setState(() {
                doTap = true;
                widget.onLongPressFunction?.call();
              });
            });
          },
          onLongPressEnd: (details) {
            setState(() {
              doTap = false;
              timer!.cancel();
              widget.onLongPressEndFunction?.call();
            });
          },
          child: Stack(
            children: [
              Stack(
                children: [
                  Image.asset(
                    widget.imagePath,
                    fit: BoxFit.fill,
                  ),
                  AnimatedOpacity(
                      opacity: doTap ? 1.0 : 0.0,
                      duration: const Duration(microseconds: 1),
                      child: Image.asset(
                        widget.onTapImagesPath,
                        fit: BoxFit.fill,
                      )),
                ],
              ),
              AnimatedOpacity(
                  opacity: check ? 1.0 : 0.0,
                  duration: const Duration(microseconds: 1),
                  child: Image.asset(
                    widget.onTapImagesPath,
                    fit: BoxFit.fill,
                  )),
            ],
          ),
        ));
  }
}
