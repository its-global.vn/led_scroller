import 'package:flutter/material.dart';

class SelectColorDialogScreen extends StatelessWidget {
  final int numberColor;
  final String title;
  const SelectColorDialogScreen(
      {Key? key, required this.numberColor, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: numberColor == 2
          ? Container(
              height: 182,
              width: 306,
              color: Colors.grey,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Material(
                      child: Container(
                        height: 70,
                        color: Colors.black,
                        child: Center(
                            child: Text(title,
                                style: const TextStyle(
                                    color: Colors.white, fontSize: 20))),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Row(
                      children: [
                        GestureDetector(
                          child: Container(
                            color: Colors.red,
                            height: 50,
                            width: 50,
                          ),
                          onTap: () => Navigator.pop(context, Colors.red),
                        ),
                        GestureDetector(
                          child: Container(
                            color: Colors.orange,
                            height: 50,
                            width: 50,
                          ),
                          onTap: () => Navigator.pop(context, Colors.orange),
                        ),
                        GestureDetector(
                          child: Container(
                            color: Colors.yellow,
                            height: 50,
                            width: 50,
                          ),
                          onTap: () => Navigator.pop(context, Colors.yellow),
                        ),
                        GestureDetector(
                          child: Container(
                            color: Colors.green,
                            height: 50,
                            width: 50,
                          ),
                          onTap: () => Navigator.pop(context, Colors.green),
                        ),
                        GestureDetector(
                          child: Container(
                            color: Colors.blue,
                            height: 50,
                            width: 50,
                          ),
                          onTap: () => Navigator.pop(context, Colors.blue),
                        ),
                        GestureDetector(
                          child: Container(
                            color: Colors.purple,
                            height: 50,
                            width: 50,
                          ),
                          onTap: () => Navigator.pop(context, Colors.purple),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Row(
                      children: [
                        GestureDetector(
                          child: Container(
                            color: Colors.black,
                            height: 50,
                            width: 50,
                          ),
                          onTap: () => Navigator.pop(context, Colors.black),
                        ),
                        GestureDetector(
                          child: Container(
                            color: Colors.black45,
                            height: 50,
                            width: 50,
                          ),
                          onTap: () => Navigator.pop(context, Colors.black45),
                        ),
                        GestureDetector(
                          child: Container(
                            color: Colors.black12,
                            height: 50,
                            width: 50,
                          ),
                          onTap: () => Navigator.pop(context, Colors.black12),
                        ),
                        GestureDetector(
                          child: Container(
                            color: Colors.white24,
                            height: 50,
                            width: 50,
                          ), onTap: () => Navigator.pop(context, Colors.white24),
                        ),
                        GestureDetector(
                          child: Container(
                            color: Colors.white60,
                            height: 50,
                            width: 50,
                          ),
                          onTap: () => Navigator.pop(context, Colors.white60),
                        ),
                        GestureDetector(
                          child: Container(
                            color: Colors.white,
                            height: 50,
                            width: 50,
                          ),
                          onTap: () => Navigator.pop(context, Colors.white),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          : Container(
              height: 402,
              width: 306,
              color: Colors.grey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 2.0, left: 2.0),
                    child: Material(
                      child: Container(
                        height: 70,
                        color: Colors.black,
                        child: Center(
                            child: Text(title,
                                style: const TextStyle(
                                    color: Colors.white, fontSize: 20))),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Row(
                      children: [
                        for (int i = 1; i <= 6; i++)
                          GestureDetector(
                            child: Container(
                              color: Colors.red[(7 - i) * 100],
                              height: 50,
                              width: 50,
                            ),
                            onTap: () => Navigator.pop(
                                context, Colors.red[(7 - i) * 100]),
                          ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Row(
                      children: [
                        for (int i = 1; i <= 6; i++)
                          GestureDetector(
                            child: Container(
                              color: Colors.orange[(7 - i) * 100],
                              height: 50,
                              width: 50,
                            ),
                            onTap: () => Navigator.pop(
                                context, Colors.orange[(7 - i) * 100]),
                          ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Row(
                      children: [
                        for (int i = 1; i <= 6; i++)
                          GestureDetector(
                            child: Container(
                              color: Colors.yellow[(7 - i) * 100],
                              height: 50,
                              width: 50,
                            ),
                            onTap: () => Navigator.pop(
                                context, Colors.yellow[(7 - i) * 100]),
                          ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Row(
                      children: [
                        for (int i = 1; i <= 6; i++)
                          GestureDetector(
                            child: Container(
                              color: Colors.green[(7 - i) * 100],
                              height: 50,
                              width: 50,
                            ),
                            onTap: () => Navigator.pop(
                                context, Colors.green[(7 - i) * 100]),
                          ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Row(
                      children: [
                        for (int i = 1; i <= 6; i++)
                          GestureDetector(
                            child: Container(
                              color: Colors.blue[(7 - i) * 100],
                              height: 50,
                              width: 50,
                            ),
                            onTap: () => Navigator.pop(
                                context, Colors.blue[(7 - i) * 100]),
                          ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Row(
                      children: [
                        for (int i = 1; i <= 6; i++)
                          GestureDetector(
                            child: Container(
                              color: Colors.purple[(7 - i) * 100],
                              height: 50,
                              width: 50,
                            ),
                            onTap: () => Navigator.pop(
                                context, Colors.purple[(7 - i) * 100]),
                          ),
                      ],
                    ),
                  )
                ],
              ),
            ),
    );
  }
}
