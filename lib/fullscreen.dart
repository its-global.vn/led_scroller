import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:marquee/marquee.dart';

class FullScreen extends StatefulWidget {
  final bool isBlink;
  final bool isStop;
  final String title;
  final double size;
  final Color textColor;
  final Color backgroundColor;
  final TextDirection textDirection;
  final AnimationController animationController;
  final double sliderValue;
  const FullScreen({
    Key? key,
    required this.isBlink,
    required this.title,
    required this.size,
    required this.textColor,
    required this.backgroundColor,
    required this.textDirection,
    required this.isStop,
    required this.animationController,
    required this.sliderValue,
  }) : super(key: key);

  @override
  State<FullScreen> createState() => _FullScreenState();
}

class _FullScreenState extends State<FullScreen> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
  }

// Step 3
  @override
  dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: widget.backgroundColor,
            child: widget.isBlink
                ? FadeTransition(
                    opacity: widget.animationController, child: sliderText())
                : sliderText(),
          ),
          // const DotNetwork(),
          SizedBox(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Image.asset(
              "assets/images/grid_medium.png",
              fit: BoxFit.cover,
            ),
          ),
        ],
      ),
    );
  }

  Widget sliderText() {
    return (widget.isStop || widget.title == "")
        ? Center(
            child: Text(
              widget.title,
              overflow: TextOverflow.clip,
              softWrap: false,
              maxLines: 1,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: widget.textColor,
                fontSize: widget.size,
              ),
            ),
          )
        : Marquee(
            key: Key(widget.title),
            text: widget.title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: widget.textColor,
              fontSize: widget.size,
            ),
            scrollAxis: Axis.horizontal,
            crossAxisAlignment: CrossAxisAlignment.center,
            blankSpace: MediaQuery.of(context).size.width,
            velocity: widget.sliderValue*1.5,
            pauseAfterRound: Duration.zero,
            textDirection: widget.textDirection,
            startAfter: const Duration(seconds: 1),
          );
  }
}
