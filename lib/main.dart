import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:led_scroller/custom_button.dart';
import 'package:led_scroller/fullscreen.dart';
import 'package:led_scroller/select_color_dialog_screen.dart';
import 'package:led_scroller/square_slider.dart';
import 'package:marquee/marquee.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  Timer? timer;
  String currentString = " ";
  TextDirection directionRTL = TextDirection.ltr;
  Color? backgroundColor;
  Color? textColor;
  double? fontSize;
  bool isStop = false;
  int currentButton = 3;
  double sliderValue = 100;
  int currentSelectedIndex = 1;
  late AnimationController _animationController;
  bool isBlink = false;

  @override
  void initState() {
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 100));
    _animationController.repeat(reverse: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.black,
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: height * 1 / 5,
              width: width,
            ),
            Stack(
              children: [
                Container(
                  height: height * 1 / 4,
                  width: width,
                  color: backgroundColor ?? Colors.grey,
                  child: isBlink
                      ? FadeTransition(
                          opacity: _animationController, child: sliderText())
                      : sliderText(),
                ),
                // const DotNetwork(),
                SizedBox(
                  height: height * 1 / 4,
                  width: width,
                  child: Image.asset(
                    "assets/images/grid_medium.png",
                    fit: BoxFit.cover,
                  ),
                ),
                const Positioned(
                    right: 1,
                    child: Text(
                      "Preview",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.yellow,
                          fontSize: 22),
                    ))
              ],
            ),
            Padding(
                padding: const EdgeInsets.only(
                    bottom: 15, left: 15, right: 15, top: 30),
                child: SizedBox(
                  height: height * 1 / 10,
                  child: TextFormField(
                    decoration: const InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      hintText: "Enter your message here !",
                      hintStyle: TextStyle(color: Colors.grey),
                      border: OutlineInputBorder(
                          gapPadding: 5.0,
                          borderSide: BorderSide(
                            color: Colors.transparent,
                            width: 5,
                          )),
                      focusedBorder: OutlineInputBorder(
                        gapPadding: 5.0,
                        borderSide: BorderSide(
                          color: Colors.orange,
                          width: 5,
                        ),
                      ),
                    ),
                    onChanged: (value) {
                      setState(() {
                        currentString = value;
                      });
                    },
                  ),
                )),
            Padding(
              padding: const EdgeInsets.all(10),
              child: SingleChildScrollView(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: CustomButton(
                        imagePath: "assets/images/left.jpg",
                        onTapImagesPath: "assets/images/tapped_left.jpg",
                        checkIndex: currentSelectedIndex,
                        thisButtonIndex: 1,
                        duration: 0,
                        onTap: () {
                          setState(() {
                            isStop = false;
                            directionRTL = TextDirection.ltr;
                            currentSelectedIndex = 1;
                          });
                        },
                      ),
                    ),
                    Expanded(
                      child: CustomButton(
                        imagePath: "assets/images/right.jpg",
                        onTapImagesPath: "assets/images/tapped_right.jpg",
                        checkIndex: currentSelectedIndex,
                        thisButtonIndex: 2,
                        duration: 0,
                        onTap: () {
                          setState(() {
                            isStop = false;
                            directionRTL = TextDirection.rtl;
                            currentSelectedIndex = 2;
                          });
                        },
                      ),
                    ),
                    Expanded(
                      child: CustomButton(
                        imagePath: "assets/images/stop.jpg",
                        onTapImagesPath: "assets/images/tapped_stop.jpg",
                        checkIndex: currentSelectedIndex,
                        thisButtonIndex: 3,
                        duration: 0,
                        onTap: () {
                          setState(() {
                            isStop = true;
                            currentSelectedIndex = 3;
                          });
                        },
                      ),
                    ),
                    Expanded(
                      child: CustomButton(
                        imagePath: "assets/images/subtract.jpg",
                        onTapImagesPath: "assets/images/tapped_subtract.jpg",
                        onTap: () {
                          setState(() {
                            fontSize = checkSize(checkSize(fontSize) - 2);
                          });
                        },
                        onLongPressFunction: () {
                          setState(() {
                            fontSize =
                                fontSize = checkSize(checkSize(fontSize) - 2);
                          });
                        },
                        onLongPressEndFunction: () {

                        },
                      ),
                    ),
                    Expanded(
                      child: CustomButton(
                        imagePath: "assets/images/add.jpg",
                        onTapImagesPath: "assets/images/tapped_add.jpg",
                        onTap: () {
                          setState(() {
                            fontSize = checkSize(checkSize(fontSize) + 2);
                          });
                        },
                        onLongPressFunction: () {
                          setState(() {
                            fontSize = checkSize(checkSize(fontSize) + 2);
                          });
                        },
                        onLongPressEndFunction: () {

                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(5),
              child: Row(
                children: [
                  Expanded(
                    flex:1,
                    child: CustomButton(
                      imagePath: "assets/images/text_color.jpg",
                      onTapImagesPath: "assets/images/text_color.jpg",
                      onTap: () {
                        setState(() {
                          _navigateAndSelectTextColor(
                                  context, 6, "Text Color Picker")
                              .then((value) => {setState(() {})});
                        });
                      },
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: CustomButton(
                      imagePath: "assets/images/blink.jpg",
                      onTapImagesPath: "assets/images/tapped_blink.jpg",
                      setImage: true,
                      onTap: () {
                        isBlink = !isBlink;
                        setState(() {});
                      },
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: SliderTheme(
                      data: SliderThemeData(
                        inactiveTrackColor: Colors.grey,
                        activeTrackColor: Colors.yellow,
                        trackHeight: 10,
                        thumbShape: SquareSliderComponentShape(),
                      ),
                      child: Slider(
                        value: sliderValue,
                        min: 100,
                        max: 500,
                        onChanged: (value) {
                          setState(() {
                            sliderValue = value;
                          });
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomButton(
                      imagePath: "assets/images/background_color.jpg",
                      onTapImagesPath: "assets/images/background_color.jpg",
                      onTap: () {
                        _navigateAndSelectBackgroundColor(
                                context, 2, "Background Color Picker")
                            .then((value) => {setState(() {})});
                      }),
                  CustomButton(
                    imagePath: "assets/images/start.jpg",
                    onTapImagesPath: "assets/images/tapped_start.jpg",
                    borderRadius: 15,
                    duration: 30,
                    width: 150,
                    onTap: () async {
                      FocusScope.of(context).unfocus();
                      await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => FullScreen(
                              title: currentString,
                              backgroundColor: backgroundColor ?? Colors.grey,
                              size: checkSize(fontSize)*height/width,
                              textColor: textColor ?? Colors.pink,
                              isBlink: isBlink,
                              textDirection: directionRTL,
                              isStop: isStop,
                              animationController: _animationController,
                              sliderValue: sliderValue,
                            ),
                          ));
                      SystemChrome.setPreferredOrientations([
                        DeviceOrientation.portraitUp,
                        DeviceOrientation.portraitDown,
                      ]);
                    },
                    onLongPressFunction: () {

                    },
                    onLongPressEndFunction: () async {
                      FocusScope.of(context).unfocus();
                      await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => FullScreen(
                              title: currentString,
                              backgroundColor: backgroundColor ?? Colors.grey,
                              size: checkSize(fontSize)*height/width,
                              textColor: textColor ?? Colors.pink,
                              isBlink: isBlink,
                              textDirection: directionRTL,
                              isStop: isStop,
                              animationController: _animationController,
                              sliderValue: sliderValue,
                            ),
                          ));
                      SystemChrome.setPreferredOrientations([
                        DeviceOrientation.portraitUp,
                        DeviceOrientation.portraitDown,
                      ]);
                    },
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: const Icon(
                      Icons.more_vert,
                      color: Colors.white,
                      size: 40,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  double checkSize(double? size) {
    if (size == null || size < 50) {
      return 50;
    } else if (size > 150) {
      return 150;
    } else {
      return size;
    }
  }

  Future<void> _navigateAndSelectTextColor(
      BuildContext context, int number, String string) async {
    final selectedColor = await showDialog(
      context: context,
      builder: (context) =>
          SelectColorDialogScreen(numberColor: number, title: string),
    );
    textColor = selectedColor;
  }

  Future<void> _navigateAndSelectBackgroundColor(
      BuildContext context, int number, String string) async {
    final selectedColor = await showDialog(
      context: context,
      builder: (context) =>
          SelectColorDialogScreen(numberColor: number, title: string),
    );
    backgroundColor = selectedColor;
  }

  Widget sliderText() {
    return (isStop || currentString == "")
        ? Center(
            child: Text(
              currentString,
              overflow: TextOverflow.clip,
              softWrap: false,
              maxLines: 1,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: textColor ?? Colors.pink,
                fontSize: checkSize(fontSize),
              ),
            ),
          )
        : Marquee(
            key: Key(currentString),
            text: currentString,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: textColor ?? Colors.pink,
              fontSize: checkSize(fontSize),
            ),
            scrollAxis: Axis.horizontal,
            crossAxisAlignment: CrossAxisAlignment.center,
            blankSpace: MediaQuery.of(context).size.width,
            velocity: sliderValue,
            pauseAfterRound: Duration.zero,
            textDirection: directionRTL,
            startPadding: 0,
          );
  }
}
